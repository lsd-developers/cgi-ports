#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
# import cgitb
# cgitb.enable()

import cgi
form = cgi.FieldStorage()

import sys
import os
import re

try:
    print('Content-Type: text/html\n')

    import lib.message as message
    message.LOG_FILE = os.getcwd() + '/cgi.log'
    import lib.config as config
    config.CACHE_DIR = os.getcwd() + '/cache'
    import lib.misc as misc
    import lib.database as database

    page_content = ''
    for target_name in database.remote_all(basename=True):
        if not misc.string_search(form.getvalue('regexp'), target_name):
            continue

        target_version = database.remote_metadata(target_name, 'version')
        target_description = database.remote_metadata(target_name, 'description')

        page_content += '<tr>\n'
        page_content += '<td style="width:150px"><a href="info.py?target=' + target_name + '">' + target_name + '</a></td>\n'
        page_content += '<td>' + target_version + '</td>\n'
        page_content += '<td>' + target_description + '</td>\n'
        page_content += '</tr>\n\n'

    print(misc.file_read('resources/search.html').replace('<!-- CONTENT -->', page_content))
except Exception as detail:
    message.critical('Unexpected error', detail)
    sys.exit(1)

#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
# import cgitb
# cgitb.enable()

import sys
import os


try:
    print('Content-Type: text/html\n')

    import lib.message as message
    message.LOG_FILE = os.getcwd() + '/cgi.log'
    import lib.config as config
    config.CACHE_DIR = os.getcwd() + '/cache'
    import lib.misc as misc

    if os.path.isfile('.stop'):
        print(misc.file_read('resources/maintenance.html'))
    else:
        print(misc.file_read('resources/index.html'))
except Exception as detail:
    message.critical('Unexpected error', detail)
    sys.exit(1)

#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
# import cgitb
# cgitb.enable()

import cgi
form = cgi.FieldStorage()

import sys
import os


try:
    print('Content-Type: text/html\n')

    import lib.message as message
    message.LOG_FILE = os.getcwd() + '/cgi.log'
    import lib.config as config
    config.CACHE_DIR = os.getcwd() + '/cache'
    import lib.misc as misc
    import lib.database as database

    page_content = ''
    target_name = form.getvalue('target')
    target_version = database.remote_metadata(target_name, 'version')
    target_description = database.remote_metadata(target_name, 'description')
    target_depends = database.remote_metadata(target_name, 'depends')
    target_makedepends = database.remote_metadata(target_name, 'makedepends')
    target_checkdepends = database.remote_metadata(target_name, 'checkdepends')
    target_sources = database.remote_metadata(target_name, 'sources')
    target_options = misc.string_convert(database.remote_metadata(target_name, 'options'))
    target_backup = misc.string_convert(database.remote_metadata(target_name, 'backup'))

    page_content += '<tr>\n'
    page_content += '<td style="width:150px">Name</td>\n'
    page_content += '<td>' + target_name + '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Version</td>\n'
    page_content += '<td>' + target_version + '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Description</td>\n'
    page_content += '<td>' + target_description + '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Depends</td>\n'
    page_content += '<td>'
    for depends in target_depends:
        page_content += '<a href="info.py?target=' + depends + '">' + depends + '</a> '
    page_content += '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Make depends</td>\n'
    page_content += '<td>'
    for depends in target_makedepends:
        page_content += '<a href="info.py?target=' + depends + '">' + depends + '</a> '
    page_content += '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Check depends</td>\n'
    page_content += '<td>'
    for depends in target_checkdepends:
        page_content += '<a href="info.py?target=' + depends + '">' + depends + '</a> '
    page_content += '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Sources</td>\n'
    page_content += '<td>'
    for source in target_sources:
        if source.startswith('http') or source.startswith('https') \
            or source.startswith('ftp') or source.startswith('sftp'):
            page_content += '<a href="' + source + '">' + os.path.basename(source) + '</a> '
        else:
            page_content += source + ' '
    page_content += '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Options</td>\n'
    page_content += '<td>' + target_options + '</td>\n'
    page_content += '</tr>\n\n'

    page_content += '<tr>\n'
    page_content += '<td>Backup</td>\n'
    page_content += '<td>' + target_backup + '</td>\n'
    page_content += '</tr>\n\n'

    print(misc.file_read('resources/info.html').replace('<!-- CONTENT -->', page_content))
except Exception as detail:
    message.critical('Unexpected error', detail)
    sys.exit(1)
